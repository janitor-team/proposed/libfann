#!/usr/bin/make -f
# Based on upstream's makefile for the examples, see $SRC/examples/Makefile
# See the README in this directory for instructions on how to run this

CC=gcc

TARGETS = xor_train xor_test simple_train steepness_train simple_test robot mushroom momentums cascade_train scaling_test scaling_train

all: $(TARGETS)

%: /usr/share/doc/libfann-dev/examples/%.c
	$(CC) -O3 $< -o $@ -lm -lfann

clean:
	rm -f $(TARGETS) xor_fixed.data *.net

runtest: $(TARGETS)
	@echo
	@echo Training network
	./xor_train

	@echo
	@echo Testing network with floats
	./xor_test
